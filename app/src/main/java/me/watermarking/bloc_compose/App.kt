package me.watermarking.bloc_compose

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import me.watermarking.bloc_compose.ui.theme.Bloc_composeTheme

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}


@Composable
fun DefaultPreview() {
    Bloc_composeTheme {
        Greeting("Android")
    }
}