package me.watermarking.bloc_compose.counter.cubic

import android.os.Parcelable
import androidx.versionedparcelable.ParcelField
import com.ptrbrynt.kotlin_bloc.core.Cubit
import kotlinx.parcelize.Parcelize

@Parcelize
class CountCubit:Cubit<Int>(0),Parcelable{
    suspend fun increment()=emit(state +1)
}