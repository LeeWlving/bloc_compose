package me.watermarking.bloc_compose.counter

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.ptrbrynt.kotlin_bloc.compose.BlocComposer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun Counter(
    scope: CoroutineScope = rememberCoroutineScope(),
    cubit: CounterCubit = rememberSaveable { CounterCubit() },
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Counter") },
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    scope.launch { cubit.increment() }
                },
            ) {
                Icon(Icons.Default.Add, "Increment")
            }
        }
    ) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center,
        ) {
            BlocComposer(bloc = cubit) {
                Text("$it", style = MaterialTheme.typography.h4)
            }
        }
    }
}

@Preview
@Composable
fun CounterPreview() {
    Counter()
}