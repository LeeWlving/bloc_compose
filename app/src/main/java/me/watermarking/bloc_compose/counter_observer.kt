package me.watermarking.bloc_compose


import android.util.Log
import com.ptrbrynt.kotlin_bloc.core.BlocBase
import com.ptrbrynt.kotlin_bloc.core.BlocObserver
import com.ptrbrynt.kotlin_bloc.core.Change

class CounterObserver : BlocObserver(){
    override fun <B : BlocBase<State>, State> onChange(bloc: B, change: Change<State>) {
        super.onChange(bloc, change)
        Log.i(bloc::class.simpleName,change.toString())
    }
}